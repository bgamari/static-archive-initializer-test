set -ex

g++ -c hi.cpp $CXXFLAGS
ar cr hi.a hi.o

g++ -c main.cpp $CXXFLAGS

gcc main.o hi.a -o main $LDFLAGS
